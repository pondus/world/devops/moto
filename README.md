Moto
====

Welcome to the source of Moto, an open-source book on the HashiCorp stack.

This site is open to contributions through PR/MR. There is no contribution guidelines out of thriving in submitting focused and constructive input. Please also refer to the short [introduction post][ip] for context.

The preferred format for book contributions are the Markdown flavours supported by the [Jekyll framework][Jekyll] ![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg).


[ip]: https://pondus.gitlab.io/world/devops/moto/why/what/draft/2021/01/11/moto
[Jekyll]: http://jekyllrb.com/
