---
layout: page
title: About
permalink: /about/
---

The HashiCorp stack has been our friend for a decade, since Vagrant. There is documentation, and the popularity of the stack stirs people into writing and contributing. The community is alive!

We feel some more guidance across the stack could do marvels, with recipes and tips the DevOps plane would enjoy and build upon. This site is just about that: A cookbook and some hand-holding for DevOps across the HashiCorp stack.

On to the adventure.

Interested in the source or contributing? Everything is [available](https://gitlab.com/pondus/world/devops/moto/) online.

EP & TW
