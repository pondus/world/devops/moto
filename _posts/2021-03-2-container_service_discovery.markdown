---
layout: post
title:  "Service Discovery with Docker Containers"
date:   2021-03-02 00:00:00 +00:00
categories: networking container docker discovery service
---

What we know from [Docker](http://docs.docker.oeynet.com/engine/userguide/networking/default_network/configure-dns/):

> Note: If you need access to a host’s localhost resolver, you must modify your DNS service on the host to listen on a non-localhost address that is reachable from within the container.

And modifying the `config.json` does not work. It has to be `/etc/docker/daemon.json`, or it is ignored. Posts like [this one](https://robinwinslow.uk/fix-docker-networking-dns) clarify this point, although many posts just mention either `--dns` or `config.json`.
